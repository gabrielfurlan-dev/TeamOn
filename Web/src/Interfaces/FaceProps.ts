import { EHumorStatus } from './../enums/EHumorStatus';

export interface FacesProps {
    width?: number,
    height?: number
    humor?: EHumorStatus
}
