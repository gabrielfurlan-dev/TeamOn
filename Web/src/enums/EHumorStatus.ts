export enum EHumorStatus{
    Happy,
    HalfHappy,
    Emotionless,
    HalfSad,
    Sad
}