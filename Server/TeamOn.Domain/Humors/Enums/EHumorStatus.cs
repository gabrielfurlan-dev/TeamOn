namespace TeamOn.Domain.Humors.Enums
{
    public enum EHumorStatus
    {
        Happy,
        HalfHappy,
        Emotionless,
        HalfSad,
        Sad
    }
}